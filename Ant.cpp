/*
161044002
Mumtaz DANACI
ANT CLASS 
*/
#include "Ant.h"
#include "PoisonousAnt.h"


//constructors
Ant::Ant(GamePtr currGame, int x, int y): Organism(currGame, x, y){
	timeTillBreed = ANT_BREED_TIME;
}
//breed redefine
void Ant::breed(){
	
	if (timeTillBreed > 0) return;
	vector<int> vMoves = getMovesToEmptyCells(x, y);
	if (vMoves.size() == 0) return;
	int randomMove = vMoves[currGame->generateRandomNumber(0, vMoves.size() - 1)];
	int tempX = x;
	int tempY = y;
	getCoordinate(tempX, tempY, randomMove);
	int number = currGame->generateRandomNumber(0, 100);
	
	
	if(number < 50)
		currGame->world[tempX][tempY] = new Ant(currGame, tempX, tempY);
	else
		currGame->world[tempX][tempY] = new PoisonousAnt(currGame, tempX, tempY);

	timeTillBreed = ANT_BREED_TIME;
}

