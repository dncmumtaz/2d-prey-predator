/*
161044002
Mumtaz DANACI
ANT CLASS HEADER
*/

#ifndef ANT_H
#define ANT_H


#include "Organism.h"

#include <string>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

//Ant class
class Ant: public Organism {

public:
	Ant(): Organism(){}
	Ant(GamePtr currGame, int x, int y);
	void breed();
	int getType()
	{
		return ANT;
	}

};

#endif /*ANT_H*/

