#include "Doodlebug.h"


using namespace std;



vector<int> Doodlebug::getMovesToAnts(int x, int y) const {
	vector<int> movesToAnts;

	int tempX, tempY;
	for (int move = LEFT; move <= UP; move++){
		tempX = x;
		tempY = y;
		getCoordinate(tempX, tempY, move);
		if (!isValidCoordinate(tempX, tempY)) continue;
		if (currGame->world[tempX][tempY] == nullptr) continue;
		if (currGame->world[tempX][tempY]->getType() == ANT) 
			movesToAnts.push_back(move);
		
	}
	return movesToAnts;
}

vector<int> Doodlebug::getMovesToPoisonousAnts(int x, int y) const {
	vector<int> movesToPoisonousAnts;

	int tempX, tempY;
	for (int move = LEFT; move <= UP; move++){
		tempX = x;
		tempY = y;
		getCoordinate(tempX, tempY, move);
		if (!isValidCoordinate(tempX, tempY)) continue;
		if (currGame->world[tempX][tempY] == nullptr) continue;
		if (currGame->world[tempX][tempY]->getType() == POISONOUSANT) 
			movesToPoisonousAnts.push_back(move);
		
	}
	return movesToPoisonousAnts;
}


Doodlebug::Doodlebug(GamePtr currGame, int x, int y): Organism(currGame, x, y){
	timeTillStarve = DOODLEBUG_STARVE_TIME;
	timeTillBreed = DOODLEBUG_BREED_TIME;
	timeTillPoison = 	DOODLEBUG_POISON_TIME;
}

void Doodlebug::breed(){
	if (timeTillBreed > 0) return;
	vector<int> validMoves = getMovesToEmptyCells(x, y);
	if (validMoves.size() == 0) return;
	int randomMove = validMoves[currGame->generateRandomNumber(0, validMoves.size() - 1)];
	int newX = x;
	int newY = y;
	getCoordinate(newX, newY, randomMove);
	currGame->world[newX][newY] = new Doodlebug(currGame, newX, newY);
	timeTillBreed = DOODLEBUG_BREED_TIME;
}

void Doodlebug::move(){
	if (timeStepCount == currGame->timeStepCount) return;
	vector<int> movesToAnts = getMovesToAnts(x, y);
	vector<int> movesToPoisonousAnts = getMovesToPoisonousAnts(x, y);


	if (movesToAnts.size() == 0 && movesToPoisonousAnts.size() == 0 ){
		Organism::move();
		timeTillStarve--;
		return;
	}
	else if ( movesToAnts.size() != 0 && movesToPoisonousAnts.size() == 0 )
	{
		timeStepCount++;
		timeTillStarve = DOODLEBUG_STARVE_TIME;
		int randomMove = movesToAnts[currGame->generateRandomNumber(0, movesToAnts.size() - 1)];
		int antX = x;
		int antY = y;
		getCoordinate(antX, antY, randomMove);
		delete currGame->world[antX][antY];
		currGame->world[antX][antY] = this;
		currGame->world[x][y] = nullptr;
		x = antX;
		y = antY;
	}
	else if(movesToAnts.size() == 0 && movesToPoisonousAnts.size() != 0 )
	{
		timeStepCount++;
		timeTillPoison--;
		int randomMove = movesToPoisonousAnts[currGame->generateRandomNumber(0, movesToPoisonousAnts.size() - 1)];
		int poisonousAntX = x;
		int poisonousAntY = y;
		getCoordinate(poisonousAntX, poisonousAntY, randomMove);
		delete currGame->world[poisonousAntX][poisonousAntY];
		currGame->world[poisonousAntX][poisonousAntY] = this;
		currGame->world[x][y] = nullptr;
		x = poisonousAntX;
		y = poisonousAntY;
	}
	else if (movesToAnts.size() != 0 && movesToPoisonousAnts.size() != 0 )
	{
		srand(time(NULL));
		bool whichOne = rand()%2;
		if(whichOne)
		{
			timeStepCount++;
			timeTillStarve = DOODLEBUG_STARVE_TIME;
			int randomMove = movesToAnts[currGame->generateRandomNumber(0, movesToAnts.size() - 1)];
			int antX = x;
			int antY = y;
			getCoordinate(antX, antY, randomMove);
			delete currGame->world[antX][antY];
			currGame->world[antX][antY] = this;
			currGame->world[x][y] = nullptr;
			x = antX;
			y = antY;
		}
		else
		{
			timeStepCount++;
			timeTillPoison-- ;
			int randomMove = movesToPoisonousAnts[currGame->generateRandomNumber(0, movesToPoisonousAnts.size() - 1)];
			int poisonousAntX = x;
			int poisonousAntY = y;
			getCoordinate(poisonousAntX, poisonousAntY, randomMove);
			delete currGame->world[poisonousAntX][poisonousAntY];
			currGame->world[poisonousAntX][poisonousAntY] = this;
			currGame->world[x][y] = nullptr;
			x = poisonousAntX;
			y = poisonousAntY;
		}
	}
}