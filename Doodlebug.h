/*
161044002
Mumtaz DANACI
DOODLEBUG CLASS HEADER
*/
#ifndef DOODLEBUG_H
#define DOODLEBUG_H

#include "Organism.h"

#include <iostream>
#include <vector>
#include <string>

using namespace std;


//organism class
class Doodlebug: public Organism {
public:
  Doodlebug(): Organism(), timeTillStarve(0){}
  Doodlebug(GamePtr currGame, int x, int y);
  void breed();
  void move();
  int getType(){ return DOODLEBUG; }
  bool starves(){ return timeTillStarve == 0; }
  bool poisoning(){ return timeTillPoison == 0; }


private:
  int timeTillStarve;
  int timeTillPoison;


  vector<int> getMovesToAnts(int x, int y) const;
  vector<int> getMovesToPoisonousAnts(int x, int y) const ;


};
#endif /*DOODLEBUG_H*/


