#include "Doodlebug.h"
#include "Ant.h"
#include "Game.h"
#include "PoisonousAnt.h"



Game::Game()
{
    srand(time(NULL));
    timeStepCount = 0;
    
    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            world[x][y] = nullptr;
        }
    }
}

int Game::generateRandomNumber(int sRange, int eRange) const 
{
    return rand() % (eRange - sRange + 1) + sRange;
}



void Game::startGame()
{
    int x,y;
    int doodlebugCount = 0;
    int antCount = 0;
    int PoisonousAntCount = 0;

    while (doodlebugCount < INIT_DOODLEBUG_COUNT){
        x = generateRandomNumber(0, WORLD_DIMENSION - 1);
        y = generateRandomNumber(0, WORLD_DIMENSION - 1);
        if (world[x][y] != nullptr) continue;
        world[x][y] = new Doodlebug(this, x, y);
        doodlebugCount++;
    }

    while (antCount < INIT_ANT_COUNT){
        x = generateRandomNumber(0, WORLD_DIMENSION - 1);
        y = generateRandomNumber(0, WORLD_DIMENSION - 1);
        if (world[x][y] != nullptr) continue;
        world[x][y] = new Ant(this, x, y);
        antCount++;
    }
    while (PoisonousAntCount < INIT_POISONOUSANT_COUNT){
        x = generateRandomNumber(0, WORLD_DIMENSION - 1);
        y = generateRandomNumber(0, WORLD_DIMENSION - 1);
        if (world[x][y] != nullptr) continue;
        world[x][y] = new PoisonousAnt(this, x, y);
        PoisonousAntCount++;
    }
}

void Game::takeTimeStep(){
    timeStepCount++;
//srand(time(NULL));
    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            if (world[x][y] == nullptr) continue;
            if (world[x][y]->getType() == DOODLEBUG)
                world[x][y]->move();
        }
    }

    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            if (world[x][y] == nullptr) continue;
            if (world[x][y]->getType() == ANT)
                world[x][y]->move();
        }
    }

    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            if (world[x][y] == nullptr) continue;
            if (world[x][y]->getType() == POISONOUSANT)
                world[x][y]->move();
        }
    }   

    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            if (world[x][y] == nullptr) continue;
            world[x][y]->breed();
        }
    }

    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            if (world[x][y] == nullptr) continue;
            if (world[x][y]->poisoning())
            {
                delete world[x][y];
                world[x][y] = nullptr;
            }
            if (world[x][y] != nullptr && world[x][y]->starves())
            {
                delete world[x][y];
                world[x][y] = nullptr;
            }
        }
    }
}

void Game::printWorld() const 
{
    for (int x = 0; x < WORLD_DIMENSION; x++)
    {
        for (int y = 0; y < WORLD_DIMENSION; y++)
        {
            if (world[x][y] == nullptr)
                cout << '-';
            else if (world[x][y]->getType() == ANT) 
                cout << 'o';
            else if(world[x][y]->getType() == POISONOUSANT) 
                cout << 'c';
            else  //world[x][y]->getType() == DOODLEBUG
                cout << 'X';
        }
        cout << endl;
    }
}

