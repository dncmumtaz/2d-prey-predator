/*
161044002
Mumtaz DANACI
GAME CLASS HEADER
*/
#ifndef GAME_H
#define GAME_H

#include "Organism.h"
#include "Global.h"



class Game {

  friend class Organism;
  friend class Ant;
  friend class Doodlebug;
  friend class PoisonousAnt;

private:
  OrganismPtr world[WORLD_DIMENSION][WORLD_DIMENSION];
  int timeStepCount;
  int generateRandomNumber(int startRange, int endRange) const;

public:
  Game();
  void startGame();
  void takeTimeStep();
  void printWorld() const;

};
#endif /*GAME_H*/

