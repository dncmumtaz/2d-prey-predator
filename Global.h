#ifndef GLOBAL_H
#define GLOBAL_H

class Game;
class Organism;
class Ant;
class Doodlebug;
class PoisonousAnt;


const int LEFT = 1;/*direciton identity*/
const int RIGHT = 2;
const int DOWN = 3;
const int UP = 4;

const int WORLD_DIMENSION = 20;/*world size*/


const int INIT_DOODLEBUG_COUNT = 5;/*initial model count*/
const int INIT_ANT_COUNT = 100;
const int INIT_POISONOUSANT_COUNT = 0;

const int DOODLEBUG_BREED_TIME = 8;/*model breeds time*/
const int ANT_BREED_TIME = 3;
const int POISONOUSANT_BREED_TIME = 4;

const int DOODLEBUG_STARVE_TIME = 3;/*starve time*/
const int DOODLEBUG_POISON_TIME = 2;/*poison time for doodlebug*/

const int DOODLEBUG = 1;/*model identity*/
const int ANT = 2;
const int POISONOUSANT = 3;


typedef Organism* OrganismPtr;
typedef Game* GamePtr;

#endif 