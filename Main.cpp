#include "Game.h"
#include "Organism.h"

#include <iostream>


using namespace std;

int main() {
	
	char inputU;
	Game game;

	cout << "2-D PREDATOR PREY SIMULATION" << endl;
	cout << "Enter to start the game" << endl;
	cin.get(inputU);

	if (inputU != '\n'){
		cout << "End of simulation" << endl;
		exit(0);
	}

	game.startGame();
	cout << "This is your randomly generated game board!" << endl << endl;
	game.printWorld();
	cout << endl;

	cout << "Enter to generate a timestep" << endl;
	cin.get(inputU);

	while(inputU == '\n'){
		game.takeTimeStep();
		game.printWorld();
		cout << endl;
		cout << "Enter to generate a timestep " << endl;
		cin.get(inputU);
	}

	cout << "End of simulation" << endl;

	return 0;
}