#include "Organism.h"




vector<int> Organism::getMovesToEmptyCells(int x, int y) const {
	vector<int> movesToEmptyCells;
	int tempX, tempY;
	for (int move = LEFT; move <= UP; move++){
		tempX = x;
		tempY = y;
		getCoordinate(tempX, tempY, move);
		if (!isValidCoordinate(tempX, tempY)) continue;
		if (currGame->world[tempX][tempY] == nullptr) 
			movesToEmptyCells.push_back(move);
	}
	return movesToEmptyCells;
}

vector<int> Organism::getMovesToPossible(int x, int y) const 
{
	
	vector<int> movesToPossibleCells;
	int tempX, tempY;
	for (int move = LEFT; move <= UP; move++)
	{
		tempX = x;
		tempY = y;
		getCoordinate(tempX, tempY, move);
		if (!isValidCoordinate(tempX, tempY)) continue;
		
		movesToPossibleCells.push_back(move);
	}
	return movesToPossibleCells;
}

bool Organism::isValidCoordinate(int x, int y) const
{
	if (x < 0 || x >= WORLD_DIMENSION || y < 0 || y >= WORLD_DIMENSION)
		return false;
	return true;
}

void Organism::getCoordinate(int& x, int& y, int move) const 
{
	if (move == LEFT) x--;
	if (move == RIGHT) x++;
	if (move == DOWN) y--;
	if (move == UP) y++;
}

Organism::Organism(GamePtr currGame, int x, int y)
{
	
	this->currGame = currGame;
	this->x = x;
	this->y = y;
	timeTillBreed = 0;
	timeStepCount = currGame->timeStepCount;
}

void Organism::move()
{
	if (timeStepCount == currGame->timeStepCount) return;
	timeStepCount++;
	timeTillBreed--;
	int randomMove = currGame->generateRandomNumber(LEFT, UP);
	int newX = x;
	int newY = y;
	getCoordinate(newX, newY, randomMove);
	if (isValidCoordinate(newX, newY)){
		if (currGame->world[newX][newY] != nullptr) return;
		currGame->world[x][y] = nullptr;
		currGame->world[newX][newY] = this;
		x = newX;
		y = newY;
	}
}
