/*
161044002
Mumtaz DANACI
ORGANISM CLASS HEADER
*/




#ifndef ORGANISM_H
#define ORGANISM_H

#include "Game.h"
#include "Global.h"


#include <iostream>
#include <vector>
#include <string>




using namespace std;

class Organism {

protected:
	GamePtr currGame;
	int x;
	int y;
	int timeTillBreed;
	int timeStepCount;
  //given a coordinate of the cell (x,y), 
  //returns a list of valid moves to adjacent empty cells
	vector<int> getMovesToEmptyCells(int x, int y) const;
	vector<int> getMovesToPossible(int x, int y) const;

		bool isValidCoordinate(int x, int y) const;
  //given a valid move from grid[x][y],
  //updates x and y according to the move
		void getCoordinate(int& x, int& y, int move) const;

	public:
		Organism(): currGame(nullptr), x(0), y(0), timeTillBreed(0), timeStepCount(0){}
		Organism( GamePtr currGame, int x, int y);
		virtual void breed() = 0;
		virtual void move();
		virtual int getType() = 0;
		virtual bool starves() { return false; }
		virtual bool poisoning() { return false; }


	};

#endif /*ORGANISM_H*/