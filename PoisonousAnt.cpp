#include "PoisonousAnt.h"




PoisonousAnt::PoisonousAnt(GamePtr currGame, int x, int y): Ant(currGame, x, y){
	srand(time(NULL));
	timeTillBreed = POISONOUSANT_BREED_TIME;
}

void PoisonousAnt::breed(){
	if (timeTillBreed > 0) return;
	vector<int> validMoves = getMovesToEmptyCells(x, y);
	vector<int> validMoves2 ;
	int number2 = 0;
	int	randomMove = 0;
	if (validMoves.size() == 0) 
	{ 
		validMoves2 = getMovesToPossible(x, y);
		number2 = validMoves2[currGame->generateRandomNumber(0, validMoves2.size() - 1)];
	}
	else
	{
		randomMove = validMoves[currGame->generateRandomNumber(0, validMoves.size() - 1)];

	}
	int newX = x;
	int newY = y;
	if (validMoves.size() == 0)
		getCoordinate(newX, newY, number2);
	else
		getCoordinate(newX, newY, randomMove);
	currGame->world[newX][newY] = new PoisonousAnt(currGame, newX, newY);
	timeTillBreed = POISONOUSANT_BREED_TIME;
}
/*
ooo
oco
ooo
*/