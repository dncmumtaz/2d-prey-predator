/*
161044002
Mumtaz DANACI
POISONOUSANT CLASS HEADER
*/
#ifndef POISONOUSANT_H
#define POISONOUSANT_H



#include "Ant.h"

#include <iostream>
#include <vector>
#include <string>


using namespace std;


class PoisonousAnt: public Ant {

public:
	PoisonousAnt(): Ant(){}
	PoisonousAnt(GamePtr currGame, int x, int y);
	void breed();
	int getType()
	{
		return POISONOUSANT;
	}
};

#endif /*POISONOUSANT_H*/

