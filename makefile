MAIN = Main.cpp Organism.cpp Game.cpp Ant.cpp Doodlebug.cpp PoisonousAnt.cpp

CPP = g++

VERS = -w --std=c++11

OUT = output

all :	$(MAIN)
		$(CPP) $(MAIN) $(VERS) -o $(OUT)